package mif04.gdw.tp1.dao;

import mif04.gdw.tp1.modele.Categorie;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by ecoquery on 25/09/2016.
 */
public class CategorieDAO {
    private final EntityManager em;

    public CategorieDAO(EntityManager em) {
        this.em = em;
    }

    public List<Categorie> getAllCategories() {
        return em.createQuery("SELECT cat FROM Categorie cat", Categorie.class).getResultList();
    }

    /**
     * Créée une nouvelle catégorie en fonction de son nom ou renvoie une catégorie si elle existe déjà.
     *
     * @param nomCategorie
     * @return
     */
    public Categorie getOrCreate(String nomCategorie) {
        Categorie cat = getCategorieByNom(nomCategorie);
        if (cat == null) {
            cat = new Categorie();
            cat.setNom(nomCategorie);
            em.persist(cat);
        }
        return cat;
    }

    private Categorie getCategorieByNom(String nom) {
        TypedQuery<Categorie> q = em.createQuery("SELECT cat FROM Categorie cat WHERE cat.nom = ?1", Categorie.class);
        q.setParameter(1,nom);
        Collection<Categorie> results = q.getResultList();
        if (results.size() > 0) {
            return results.iterator().next();
        } else {
            return null;
        }
    }
}
