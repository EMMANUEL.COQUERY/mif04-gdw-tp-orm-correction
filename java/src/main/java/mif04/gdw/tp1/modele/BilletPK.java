package mif04.gdw.tp1.modele;

import java.io.Serializable;

/**
 * Created by ecoquery on 25/09/2016.
 */
public class BilletPK implements Serializable{
    public Categorie categorie;
    public String titre;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BilletPK billetPK = (BilletPK) o;

        if (!categorie.equals(billetPK.categorie)) return false;
        return titre.equals(billetPK.titre);

    }

    @Override
    public int hashCode() {
        int result = categorie.hashCode();
        result = 31 * result + titre.hashCode();
        return result;
    }
}
