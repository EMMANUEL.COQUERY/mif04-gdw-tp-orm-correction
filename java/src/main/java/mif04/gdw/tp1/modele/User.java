package mif04.gdw.tp1.modele;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by ecoquery on 25/09/2016.
 */
@Entity
@Table(name = "users")
public class User {
    @Id
    private String email;
    private String pseudo;

    /**
     * Renvoie le pseudo de l'utilisateur
     *
     * @return le pseudo
     */
    public String getPseudo() {
        return pseudo;
    }

    /**
     * Renvoie l'email de l'utilisateur
     *
     * @return
     */
    public String getEmail() {
        return email;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
