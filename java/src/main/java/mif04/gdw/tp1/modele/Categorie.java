package mif04.gdw.tp1.modele;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.*;

/**
 * Created by ecoquery on 25/09/2016.
 */
@Entity()
@Table(name = "CATEGORIE")
public class Categorie {

    private static final Logger LOG = LoggerFactory.getLogger(Categorie.class);

    @Id
    @GeneratedValue()
    @Column(name = "ID_CAT")
    private int id;

    private String nom;

    @OneToMany(mappedBy = "categorie")
    private Collection<Billet> billets = new ArrayList<>();

    /**
     * Renvoie le nom de la catégorie
     *
     * @return
     */
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Renvoie les billets attachés à cette collection
     *
     * @return
     */
    public Collection<Billet> getBillets() {
        return billets;
}

    public String getNomEncode() {
        try {
            return URLEncoder.encode(getNom(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            LOG.error("erreur d'encodage", e);
            return "erreur";
        }
    }
}
