package mif04.gdw.tp1.modele;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.persistence.*;

/**
 * Created by ecoquery on 25/09/2016.
 */
@Entity
@IdClass(BilletPK.class)
@Table(name="BILLET")
public class Billet {
    private static final Logger LOG = LoggerFactory.getLogger(Billet.class);

    @Id
    @ManyToOne
    @JoinColumn(name = "ID_CAT")
    private Categorie categorie;
    @Id
    private String titre;
    @Lob
    private String contenu;
    @ManyToOne
    private User user;

    /**
     * Renvoie l'utilisateur ayant écrit ce billet
     *
     * @return
     */
    public User getUser() {
        return user;
    }

    /**
     * Renvoie la catégorie du billet
     *
     * @return la catégorie du billet
     */
    public Categorie getCategorie() {
        return categorie;
    }

    /**
     * Renvoie le titre du billet
     *
     * @return le titre
     */
    public String getTitre() {
        return titre;
    }

    /**
     * Renvoie le texte du billet
     * @return
     */
    public String getContenu() {
        return contenu;
    }

    /**
     * Met à jour le contenu du billet
     *
     * @param contenu
     */
    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    /**
     * Le titre du billet encode pour les urls
     *
     * @return
     */
    public String getTitreEncode() {
        try {
            return URLEncoder.encode(getTitre(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            LOG.error("erreur d'encodage", e);
            return "erreur";
}
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
